#ifndef BLOCK_H
#define BLOCK_H

#include <vector>
#include <cstdint>

namespace exp
{
    using Block = std::vector<uint8_t>;
}

#endif
