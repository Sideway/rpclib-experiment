#ifndef TRANSFORM_HH
#define TRANSFORM_HH

#include <rpc/client.h>
#include <cstdint>

namespace proto
{
    namespace service
    {
        /*
         * Starts a transform process
         */
        void launch_transform(rpc::client& client, int ipid, uint32_t secret, int argc, char** argv);
    }
}

#endif
