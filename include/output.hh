#ifndef OUTPUT_HH
#define OUTPUT_HH

#include <rpc/client.h>
#include <cstdint>

namespace proto
{
    namespace service
    {
        /*
         * Starts an output process
         */
        void launch_output(rpc::client& client, int ipid, uint32_t secret, int argc, char** argv);
    }
}

#endif
