#ifndef CONFIG_H
#define CONFIG_H

#include <cstdint>

namespace proto
{
    namespace config
    {
        constexpr int port = 9999;
    };
};

#endif
