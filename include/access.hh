#ifndef ACCESS_HH
#define ACCESS_HH

#include <rpc/client.h>
#include <cstdint>

namespace proto
{
    namespace service
    {
        /*
         * Starts an access process.
         */
        void launch_access(rpc::client& client, int ipid, uint32_t secret, int argc, char** argv);
    }
}

#endif
