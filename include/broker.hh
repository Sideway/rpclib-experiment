#ifndef BROKER_HH
#define BROKER_HH

#include <string>

namespace proto
{
    namespace service
    {
        /*
         * Launches the broker.
         */
        void launch_broker(std::string path, int port);
    }
}

#endif
