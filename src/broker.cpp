#include <string>
#include <algorithm>
#include <vector>
#include <random>
#include <map>
#include <cstdint>
#include <stdexcept>
#include <fmt/core.h>
#include <rpc/server.h>

#include <unistd.h>

#include "broker.hh"

namespace proto
{

namespace service
{

struct Process
{
    Process(std::string name, int ipid, uint32_t secret)
        : name(name), ipid(ipid), secret(secret)
    {};

    std::string name;
    int ipid;
    uint32_t secret;
};

// Global data
std::vector<Process> broker_children;
std::map<int, std::deque<std::string>> transform_buffers;
std::map<int, std::deque<std::string>> output_buffers;
std::mt19937 prng;
int current_ipid = 1;

/*
 * Creates a new process from a kind (any of 'access', 'transform', 'output')
 */
template <typename ...Args>
Process process_spawn(std::string kind, Args... args)
{
    if (kind != "access" && kind != "transform" && kind != "output")
        throw std::runtime_error(fmt::format("Invalid process kind: '{}'", kind));

    int pid = fork();

    if (pid == -1)
        throw std::runtime_error("Could not fork");

    uint32_t secret = prng();

    if (pid == 0)
    {
        execl("/proc/self/exe",
                fmt::format("{}#{}", kind, current_ipid).c_str(),
                fmt::format("--{}", kind).c_str(), // argv[1] = process type
                "127.0.0.1", // argv[2] = broker address
                fmt::format("{}", current_ipid).c_str(), // argv[3] = process ipid
                fmt::format("{}", secret).c_str(), // argv[4] = process secret token
                args..., // argv[4+i] = User defined args
                NULL);

        // Child process
        throw std::runtime_error("Execve failed");
    }

    Process child(kind, current_ipid++, secret);
    fmt::print("[BROKER] Spawned '{}' process, pid: {}, ipid: {}, secret: 0x{:x}\n",
            kind, pid, child.ipid, child.secret);

    broker_children.push_back(child);

    return child;
}

/*
 * Checks whether the process is allowed to make a call.
 */
void check_access(int ipid, uint32_t secret)
{
    auto it = std::find_if(broker_children.begin(), broker_children.end(),
            [&](auto& p) {
                return (ipid == p.ipid) && (secret == p.secret);
            });

    if (it == broker_children.end())
        throw std::runtime_error(fmt::format("[BROKER] Error, could not find process with ipid {} and token 0x{:x}",
                    ipid, secret));
}

int spawn_transform(int ipid, uint32_t secret)
{
    check_access(ipid, secret);
    Process p = process_spawn("transform");

    return p.ipid;
}

int spawn_output(int ipid, uint32_t secret)
{
    check_access(ipid, secret);
    Process p = process_spawn("output");

    return p.ipid;
}

void transform_write(int ipid, uint32_t secret, int transform_ipid, std::string line)
{
    check_access(ipid, secret);
    transform_buffers[transform_ipid].push_back(line);

    fmt::print("access_write: {}\n", line);
}

void output_write(int ipid, uint32_t secret, int output_ipid, std::string line)
{
    check_access(ipid, secret);
    output_buffers[output_ipid].push_back(line);
}

std::string transform_get_line(int ipid, uint32_t secret)
{
    check_access(ipid, secret);
    auto it = transform_buffers.find(ipid);

    if (it == transform_buffers.end())
        throw std::runtime_error(fmt::format("No fifo queue for ipid {}", ipid));

    auto& [key, queue] = *it;

    if (queue.empty())
        return "";

    std::string head = queue.front();
    queue.pop_front();

    return head;
}

std::string output_get_line(int ipid, uint32_t secret)
{
    check_access(ipid, secret);
    auto it = output_buffers.find(ipid);

    if (it == output_buffers.end())
        throw std::runtime_error(fmt::format("No fifo queue for ipid {}", ipid));

    auto& [key, queue] = *it;

    if (queue.empty())
        return "";

    std::string head = queue.front();
    queue.pop_front();

    return head;
}

void launch_broker(std::string path, int port)
{
    fmt::print("[BROKER] Starting broker on port {}, file '{}'\n", port, path);

    // TODO: Change this for a serious implementation
    prng.seed(42);

    // Registering main callbacks
    rpc::server server(port);

    server.bind("spawn_transform", &spawn_transform);
    server.bind("spawn_output", &spawn_output);
    server.bind("transform_write", &transform_write);
    server.bind("transform_get_line", &transform_get_line);
    server.bind("output_write", &output_write);
    server.bind("output_get_line", &output_get_line);

    // Spawn an access process
    Process access = process_spawn("access", path.c_str());

    // XXX: How do we handle child processes dying ?
    server.run();
}

}

}
