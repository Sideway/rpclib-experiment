#include <fmt/core.h>

#include "output.hh"

namespace proto
{

namespace service
{

void launch_output(rpc::client& client, int ipid, uint32_t secret, int argc, char** argv)
{
    for (;;)
    {
        std::string line = client.call("output_get_line", ipid, secret).as<std::string>();

        if (line.empty())
            break;

        fmt::print("[OUTPUT] {}\n", line);
    }

    for (;;)
        continue;
}

}

}
