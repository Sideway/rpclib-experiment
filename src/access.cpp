#include <fmt/core.h>
#include <fstream>

#include "access.hh"

namespace proto
{

namespace service
{

void launch_access(rpc::client& client, int ipid ,uint32_t secret, int argc, char** argv)
{
    if (argc != 1)
    {
        fmt::print("usage:\n");
        fmt::print("    --access <file>\n");
        return;
    }

    std::string path = argv[0];
    fmt::print("[ACCESS] File path: {}\n", path);

    int transform_pid = client.call("spawn_transform", ipid, secret).as<int>();

    std::ifstream fs(path);
    std::string line;

    while (std::getline(fs, line))
        client.call("transform_write", ipid, secret, transform_pid, line);

    for (;;)
        continue;
}

}

}
