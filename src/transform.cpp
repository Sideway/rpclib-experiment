#include <fmt/core.h>
#include <fmt/color.h>

#include "transform.hh"

namespace proto
{

namespace service
{

void launch_transform(rpc::client& client, int ipid, uint32_t secret, int argc, char** argv)
{
    int output_ipid = client.call("spawn_output", ipid, secret).as<int>();

    for (;;)
    {
        std::string line = client.call("transform_get_line", ipid, secret).as<std::string>();

        if (line.empty())
            break;

        client.call("output_write", ipid, secret, output_ipid,
                fmt::format(fg(fmt::color::crimson), "{}", line));
    }

    for (;;)
        continue;
}

}

}
