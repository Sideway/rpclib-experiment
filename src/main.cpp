#include <string>
#include <fmt/core.h>
#include <rpc/server.h>

#include "broker.hh"
#include "access.hh"
#include "transform.hh"
#include "output.hh"
#include "config.hh"

// ./experiment --access 127.0.0.1 <ipid> <secret> [arguments]

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        fmt::print("usage: {} [mode] [options]\n", argv[0]);
        return 1;
    }

    std::string mode = argv[1];

    if (mode == "--broker")
    {
        if (argc < 3)
        {
            fmt::print("--broker <file>\n");
            return 1;
        }

        proto::service::launch_broker(argv[2], proto::config::port);
        return 0;
    }

    if (argc < 3)
    {
        fmt::print("usage: {} {} <broker address> [options]\n", argv[0], mode);
        return 1;
    }

    rpc::client client(argv[2], proto::config::port);
    int ipid = std::stoi(argv[3]);
    uint32_t secret = std::stoul(argv[4]);
    int new_argc = argc - 5;
    char** new_argv = &argv[5];

    if (mode == "--access")
        proto::service::launch_access(client, ipid, secret, new_argc, new_argv);
    else if (mode == "--transform")
        proto::service::launch_transform(client, ipid, secret, new_argc, new_argv);
    else if (mode == "--output")
        proto::service::launch_output(client, ipid, secret, new_argc, new_argv);
    else
    {
        fmt::print("invalid mode: '{}'\n", mode);
        return 1;
    }

    return 0;
}
